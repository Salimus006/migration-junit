package com.devo.migration.ut;

import com.devo.migration.controllers.UserController;
import com.devo.migration.models.User;
import com.devo.migration.services.UserService;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static com.devo.migration.tools.UserTool.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserController4Test {

    @Mock
    UserService userService;

    @Captor
    ArgumentCaptor<User> userCaptor;

    private UserController controller;

    @BeforeClass
    public static void before(){
        System.out.println("Start Run with Junit 4");
    }

    @AfterClass
    public static void after(){
        System.out.println("Finish Run with Junit 4");
    }

    @Before
    public void init() {
        controller = new UserController(userService);
        System.out.println("Before each test");
    }

   @Test
    public void saveUser(){
        User toSave = buildUser("Julien", "JJ", 1L);
        doReturn(toSave).when(this.userService).save(eq(toSave));

        EntityModel<User> entityModelResponse = this.controller.saveUser(toSave);

        Assert.assertTrue("fail", entityModelResponse != null);
    }

    @Test
    public void saveUserCaptor(){
        // mock
        User toSave = buildUser("famous", "andFamous", 7L);
        doReturn(toSave).when(this.userService).save(any(User.class));
        this.controller.saveUser(toSave);

        // check call
        verify(userService).save(userCaptor.capture());
        User saved = userCaptor.getValue();
        checkEquality(toSave, saved, true);
    }

    @Test
    public void findAll(){
        List<User> expected = buildUsers();

        // when
        doReturn(expected).when(this.userService).findAll();

        ResponseEntity<List<User>> response = this.controller.all();

        List<User> found = response.getBody();

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        assertThat("List equality without order",
                found, Matchers.containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void findByIdTest(){
        User expected = buildUser("Julien", "JJ", 2L);
        // when
        doReturn(Optional.of(expected)).when(this.userService).findById(eq(expected.getId()));

        ResponseEntity<User> response = this.controller.findById(2L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        User user = response.getBody();
        // check
        checkEquality(expected, user, true);
    }

    @Test
    public void deleteSuccessTest(){
        doNothing().when(this.userService).deleteById(eq(1L));
        ResponseEntity<Void> response = this.controller.delete(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void deleteFailTest(){
        doThrow(EmptyResultDataAccessException.class).when(this.userService).deleteById(anyLong());
        ResponseEntity<Void> response = this.controller.delete(1L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1(){
        this.controller.findUsersByName(null);
    }

    @Test
    @Ignore
    public void fakeTest(){
        Assert.assertEquals("message",1, 2);
    }
}
