package com.devo.migration.ut;

import com.devo.migration.models.User;
import com.devo.migration.repositories.UserRepository;
import com.devo.migration.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.devo.migration.tools.UserTool.*;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserRepository repository;

    private UserService service;

    @BeforeEach
    void init() {
        service = new UserService(repository);
    }

    @Test
    public void saveUser_Success() {
        User user = buildUser("coucou", "toi", 1L);

        // When
        doReturn(user).when(this.repository).save(user);

        User savedUser = service.save(user);
        checkEquality(user, savedUser, false);
    }
}
