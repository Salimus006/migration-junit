package com.devo.migration.ut;

import com.devo.migration.controllers.UserController;
import com.devo.migration.models.User;
import com.devo.migration.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.devo.migration.tools.UserTool.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserController5Test {

    @Mock
    UserService userService;

    @Captor
    ArgumentCaptor<User> userCaptor;

    private UserController controller;

    @BeforeAll
    static void before(){
        System.out.println("Start Run with Junit 5");
    }

    @BeforeEach
    void init() {
        controller = new UserController(userService);
    }

    @AfterAll
    static void finish(){
        System.out.println("End Run with Junit 5");
    }

   @Test
    void saveUser(){
        User toSave = buildUser("Julien", "JJ", 1L);
        doReturn(toSave).when(this.userService).save(eq(toSave));

        EntityModel<User> entityModelResponse = this.controller.saveUser(toSave);

        Assertions.assertTrue(entityModelResponse != null, "fail");

        /*entityModelResponse.getContent().
        // check response
        assertThat(entityModelResponse, isNotNull());
       // assertThat(entityModelResponse.getStatusCode(), equalTo(HttpStatus.CREATED));
        // check headers and URI
        HttpHeaders headers = entityModelResponse.;
        assertThat(headers, isNotNull());
        checkUri(entityModelResponse.getHeaders().getLocation(), URI.create("/users/1"));
        // check saved user
        User savedUser = entityModelResponse.getBody();
        checkEquality(toSave, savedUser, true);*/
    }

    @Test
    void saveUserCaptor(){
        // mock
        User toSave = buildUser("famous", "andFamous", 7L);
        doReturn(toSave).when(this.userService).save(any(User.class));
        this.controller.saveUser(toSave);

        // check call
        verify(userService).save(userCaptor.capture());
        User saved = userCaptor.getValue();
        checkEquality(toSave, saved, true);
    }

    @Test
    void findAll(){
        List<User> expected = buildUsers();

        // when
        doReturn(expected).when(this.userService).findAll();

        ResponseEntity<List<User>> response = this.controller.all();

        List<User> found = response.getBody();

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        assertThat("List equality without order",
                found, Matchers.containsInAnyOrder(expected.toArray()));
    }

    @Test
    void findByIdTest(){
        User expected = buildUser("Julien", "JJ", 2L);
        // when
        doReturn(Optional.of(expected)).when(this.userService).findById(eq(expected.getId()));

        ResponseEntity<User> response = this.controller.findById(2L);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        User user = response.getBody();
        // check
        checkEquality(expected, user, true);
    }

    @Test
    void deleteSuccessTest(){
        doNothing().when(this.userService).deleteById(eq(1L));
        ResponseEntity<Void> response = this.controller.delete(1L);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void deleteFailTest(){
        doThrow(EmptyResultDataAccessException.class).when(this.userService).deleteById(anyLong());
        ResponseEntity<Void> response = this.controller.delete(1L);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void test1(String firstName){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> this.controller.findUsersByName(firstName));

        assertThat(exception.getMessage(), equalTo("firstName ne peut pas être vide"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    void test2(String firstName) {
        assertThrows(IllegalArgumentException.class, () -> this.controller.findUsersByName(firstName));
    }

    @ParameterizedTest
    @CsvSource({"test,TEST", "test2,Test2"})
    void test3(String input, String expected) {
        // Ordre du check n'est pas le même entre jUnit 4 et 5
        assertEquals(input.toUpperCase(), expected.toUpperCase(), "Not Equals");
    }

    @ParameterizedTest
    @MethodSource("provideStringsForIsBlank")
    void methodSourceTest(String input, boolean expected) {
        assertEquals(expected, StringUtils.isBlank(input));
    }

    // Junit 4 : Ignore
    @Test
    @Disabled
    void fakeTest(){
        assertEquals(1, 2);
    }

    private static Stream<Arguments> provideStringsForIsBlank() {
        return Stream.of(
                Arguments.of(null, true),
                Arguments.of("", true),
                Arguments.of("  ", true),
                Arguments.of("not blank", false)
        );
    }
}
