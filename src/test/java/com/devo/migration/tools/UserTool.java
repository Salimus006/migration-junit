package com.devo.migration.tools;

import com.devo.migration.models.User;
import org.junit.jupiter.api.Assertions;

import java.net.URI;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTool {

    public static List<User> buildUsers(){
        return Arrays.asList(
                User.builder()
                        .firstName("tata")
                        .lastName("tata")
                        .birth(LocalDate.of(2020, 12, 20))
                        .build(),
                User.builder()
                        .firstName("toto")
                        .lastName("toto")
                        .birth(LocalDate.of(2020, 12, 20))
                        .build());
    }

    public static User buildUser(String firstName, String lastName, Long id){
        return User.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .birth(LocalDate.of(2020, 12, 20))
                .build();
    }

    public static void checkEquality(User expected, User actual, boolean checkId){

        assertThat(actual, notNullValue());

        if(checkId){
            assertThat(actual.getId(), notNullValue());
        }

        Assertions.assertAll(
                ()-> assertThat(actual.getFirstName(), equalTo(expected.getFirstName())),
                () -> assertThat(actual.getLastName(), equalTo(expected.getLastName())),
                () -> assertTrue(actual.getBirth().isEqual(expected.getBirth())));
    }

    public static void checkUri(URI actual, URI expected){
        assertThat(actual, notNullValue());
        assertThat(expected, notNullValue());
        assertThat(actual.compareTo(expected), equalTo(0));
    }
}
