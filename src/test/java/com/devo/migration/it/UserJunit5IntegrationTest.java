package com.devo.migration.it;

import com.devo.migration.models.User;
import com.devo.migration.repositories.UserRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.devo.migration.tools.UserTool.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class UserJunit5IntegrationTest {

    @Autowired
    UserRepository repository;

    @BeforeEach
    void initData() {
        repository.saveAll(buildUsers());
    }

    @AfterEach
    public void destroyAll(){
        repository.deleteAll();
    }

    @Test
    void save_success(){
        User expected = User.builder()
                .firstName("tata")
                .lastName("tata")
                .birth(LocalDate.of(2020, 12, 20))
                .build();
        User result = repository.save(expected);

        checkEquality(expected, result, true);
    }

    @Test
    void saveAll_success() {
        List<User> expected = buildUsers();
        List<User> found = repository.saveAll(expected);

        assertThat(expected.size(), equalTo(found.size()));

        assertThat("List equality without order",
                found, Matchers.containsInAnyOrder(expected.toArray()));
    }

    @Test
    void saveAll_fail() {
        assertThrows(InvalidDataAccessApiUsageException.class, () -> repository.saveAll(null));
    }

    @Test
    void findById_success(){

        User expected = User.builder()
                .id(1L)
                .firstName("titi")
                .lastName("titi")
                .birth(LocalDate.of(2020, 12, 20))
                .build();

        repository.save(expected);

        Optional<User> saved = repository.findById(1L);

        assertTrue(saved.isPresent());

        checkEquality(expected, saved.get(), true);
    }

    @Test
    void findFirstName_success(){

        User expected = User.builder()
                .id(1L)
                .firstName("myFamousFirstName")
                .lastName("titi")
                .birth(LocalDate.of(2020, 12, 20))
                .build();

        repository.save(expected);

        List<User> list = repository.findByFirstName("myFamousFirstName");

        assertThat(list.size(), equalTo(1));

        checkEquality(expected, list.get(0), true);
    }


    @Test
    void update_success(){

        User expected = User.builder()
                .id(1L)
                .firstName("wrongFirstName")
                .lastName("titi")
                .birth(LocalDate.of(2020, 12, 20))
                .build();

        User saved = repository.save(expected);

        saved.setFirstName("goodName");
        repository.save(saved);

        Optional<User> updatedOpt = repository.findById(1L);

        assertTrue(updatedOpt.isPresent());

        assertThat(updatedOpt.get().getFirstName(), equalTo("goodName"));
    }

    @Test
    void delete_success(){

        User toSave = User.builder()
                .id(1L)
                .firstName("wrongFirstName")
                .lastName("titi")
                .birth(LocalDate.of(2020, 12, 20))
                .build();

        User saved = repository.save(toSave);
        repository.save(saved);

        // delete
        repository.deleteById(1L);

        Optional<User> updatedOpt = repository.findById(1L);

        assertFalse(updatedOpt.isPresent());
    }
}
