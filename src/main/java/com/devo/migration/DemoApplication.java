package com.devo.migration;

import com.devo.migration.models.User;
import com.devo.migration.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserRepository repository) {
		return (args) -> {
			// save a few users
			repository.save(User.builder().firstName("test1").lastName("testLast").birth(LocalDate.of(2020, 12, 20)).build());
			List<User> users = repository.findByFirstName("test1");
			System.out.println("users with firstNam = test1");
			users.forEach(System.out::println);

			List<User> allUsers = repository.findAll();
			System.out.println("all users");
			allUsers.forEach(System.out::println);

		};
	}
}
