package com.devo.migration.repositories;

import com.devo.migration.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Retrun list of users matching with the userName
     *
     * @param firstName
     * @return List<User>
     */
    List<User> findByFirstName(String firstName);
}
