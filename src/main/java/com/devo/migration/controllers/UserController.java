package com.devo.migration.controllers;

import com.devo.migration.exceptions.UserNotFoundException;
import com.devo.migration.models.User;
import com.devo.migration.services.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("/users")
@Tag(name = "Users", description = "User's CRUD operations")
public class UserController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping()
    public EntityModel<User> saveUser(@Valid @RequestBody User user) {
        // Links
        // 1 self link
        Link selfLink = linkTo(methodOn(this.getClass()).saveUser(user)).withSelfRel();
        Link update = linkTo(methodOn(this.getClass()).putUser(user.getId(), user)).withRel("update");
        Link aggregateRoot = linkTo(methodOn(this.getClass()).all()).withRel("users");

        return EntityModel.of(service.save(user), selfLink, update, aggregateRoot);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable long id) {
        return service.findById(id).map(ResponseEntity::ok)
                .orElseThrow(UserNotFoundException::new);
    }

    @GetMapping("/filter/{firstName}")
    public ResponseEntity<List<User>> findUsersByName(@PathVariable String firstName) {
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("firstName ne peut pas être vide");
        }
        return ResponseEntity.ok(service.findByFirstName(firstName));
    }

    @GetMapping()
    public ResponseEntity<List<User>> all() {
        return ResponseEntity.ok(service.findAll());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> putUser(
            @PathVariable("id") final Long id, @RequestBody final User user) {
       /* service.findById(id).map(u -> {
            service.save(user);
            return u;
        }).orElseThrow(UserNotFoundException::new);*/
        service.findById(id).orElseThrow(UserNotFoundException::new);
        return ResponseEntity.ok(service.save(user));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){

        //service.findById(id).ifPresentOrElse(() -> service::deleteById);
        try {
            service.deleteById(id);
            return ResponseEntity.ok().build();
        }catch (EmptyResultDataAccessException e){
            return ResponseEntity.notFound().build();
        }
    }
}
