package com.devo.migration.services;

import com.devo.migration.models.User;
import com.devo.migration.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> findAll(){
        return repository.findAll();
    }

    public Optional<User> findById(Long id){
        return repository.findById(id);
    }

    public User save(User user){
        return repository.save(user);
    }

    public void deleteById(Long id){
        repository.deleteById(id);
    }

    public List<User> findByFirstName(String firstName){
        return repository.findByFirstName(firstName);
    }
}
