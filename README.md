### Java Spring boot junit migration from 4 to 5

### Create and run image from Dockerfile (Run command line in terminal)
1. cd project-folder
2. <code>docker build -t migration-demo .</code>(to create the image with name : <b>migration-demo</b>)
3. <code>docker images </code> (To check that the image migration-demo is created)
4. <code>docker run -p 8080:8080 migration-demo</code> (To run the application in attach mode) OR : <code>docker run -d -p 8080:8080 migration-demo</code> (For detached mode)
5. You can check the running container with command (<code>docker container ls</code>. You should have a container for the image migration-demo)
6. Open browser and go to the url : <a href="http://localhost:8080/swagger-ui/index.html" title="Open API">swagger</a> 
7. You can stop the container with command <code>docker stop container-id</code>

### Run container from docker-compose file (Run command line in terminal)
<code>docker compose up</code>
